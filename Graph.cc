/****************************************************************************/
/*                  Copyright 2001, Trustees of Boston University.          */
/*                               All Rights Reserved.                       */
/*                                                                          */
/* Permission to use, copy, or modify this software and its documentation   */
/* for educational and research purposes only and without fee is hereby     */
/* granted, provided that this copyright notice appear on all copies and    */
/* supporting documentation.  For any other uses of this software, in       */
/* original or modified form, including but not limited to distribution in  */
/* whole or in part, specific prior permission must be obtained from Boston */
/* University.  These programs shall not be used, rewritten, or adapted as  */
/* the basis of a commercial software or hardware product without first     */
/* obtaining appropriate licenses from Boston University.  Boston University*/
/* and the author(s) make no representations about the suitability of this  */
/* software for any purpose.  It is provided "as is" without express or     */
/* implied warranty.                                                        */
/*                                                                          */
/****************************************************************************/
/*                                                                          */
/*  Author:     Alberto Medina                                              */
/*              Anukool Lakhina                                             */
/*  Title:     BRITE: Boston university Representative Topology gEnerator   */
/*  Revision:  2.0         4/02/2001                                        */
/****************************************************************************/
/****************************************************************************/
/*                                                                          */
/*  Modified lightly to easily interface with ns-3                          */
/*  Author:     Josh Pelkey <jpelkey@gatech.edu>                            */
/*  Date: 3/02/2011                                                         */
/****************************************************************************/
#pragma implementation "Graph.h"

#include "Graph.h"

using namespace std;
namespace brite {

Graph::Graph(int n) : nodes(n), adjList(n), incList(n) {

  assert(n > 0);
  numNodes = n;
  numEdges = 0;

}


void Graph::AddNode(BriteNode* node, int index)
{

  assert(index >= 0 && index < numNodes); 

  try {

    nodes[index] = node;
    
  }

  catch (bad_alloc) {

    cout << "Graph::AddNode(): could not allocate node...\n" << flush;
    exit(0);
  
  }
}

void Graph::AddEdge(Edge *edge) {

  assert(edge != NULL);
  edges.insert(edges.end(), edge);
  numEdges++;

}

void Graph::AddAdjListNode(int n1, int n2) {

  adjList[n1].insert(adjList[n1].begin(), n2);

}

void Graph::AddIncListNode(Edge* edge) {

  assert(edge != NULL);
  int n1 = edge->GetSrc()->GetId(); 
  int n2 = edge->GetDst()->GetId(); 

  incList[n1].insert(incList[n1].begin(), edge);
  incList[n2].insert(incList[n2].begin(), edge);

}

BriteNode* Graph::GetNodePtr(int index) {

  assert(index >= 0 && index < numNodes);
  return nodes[index];

};

int Graph::GetNumNodes() { 

  return numNodes; 

}

int Graph::GetNumEdges() { 

  return numEdges; 

}

void Graph::SetNumNodes(int n) {
    
    assert(n > 0);
    numNodes = n; 

}

bool Graph::AdjListFind(int n1, int n2) {

  list<int>::iterator li;
  for (li = adjList[n1].begin(); li != adjList[n1].end(); li++) {
    if (*li == n2) {
      return 1;
    }
  }
  return 0;
}


double 
GetLength(double x1, double x2, double y1, double y2){

  double dx, dy;
  double foo;
  /*
  std::cout << "src.x = " << src->GetNodeInfo()->GetCoordX() << "\t";
  std::cout << "src.y = " << src->GetNodeInfo()->GetCoordY() << "\n";
  std::cout << "dst.x = " << dst->GetNodeInfo()->GetCoordX() << "\t";
  std::cout << "dst.y = " << dst->GetNodeInfo()->GetCoordY() << "\n";
  */

  dx = x2-x1;
  dy = y2-y1;

  //dx = (double) src->GetNodeInfo()->GetCoordX() -  (double)dst->GetNodeInfo()->GetCoordX();
  //dy = (double) src->GetNodeInfo()->GetCoordY() -  (double)dst->GetNodeInfo()->GetCoordY();

  foo = (dx*dx) + (dy*dy);
  double output = sqrt(foo);

  //std::cout << "Length of link between " << src->GetId() << " and " << dst->GetId() << " is " << output << "\n";

  return output;

}


double 
GetLength(BriteNode* src, BriteNode* dst ){

  double dx, dy;
  double foo;
  /*
  std::cout << "src.x = " << src->GetNodeInfo()->GetCoordX() << "\t";
  std::cout << "src.y = " << src->GetNodeInfo()->GetCoordY() << "\n";
  std::cout << "dst.x = " << dst->GetNodeInfo()->GetCoordX() << "\t";
  std::cout << "dst.y = " << dst->GetNodeInfo()->GetCoordY() << "\n";
  */
  dx = (double) src->GetNodeInfo()->GetCoordX() -  (double)dst->GetNodeInfo()->GetCoordX();
  dy = (double) src->GetNodeInfo()->GetCoordY() -  (double)dst->GetNodeInfo()->GetCoordY();

  foo = (dx*dx) + (dy*dy);
  double output = sqrt(foo);

  //std::cout << "Length of link between " << src->GetId() << " and " << dst->GetId() << " is " << output << "\n";

  return output;

}

double 
ABS(double v) 
{
  return v * ( (v<0) * (-1) + (v>0));
  // simpler: v * ((v>0) - (v<0))   thanks Jens
}

bool
inCircle( BriteNode* src, double circlePos_x , double circlePos_y, double R ){
  
  double dx = ABS( src->GetNodeInfo()->GetCoordX() - circlePos_x );
  if (    dx >  R ) 
    return false;

  double dy = ABS( src->GetNodeInfo()->GetCoordY() - circlePos_y);
  if (    dy >  R ) 
    return false;

  if ( dx+dy <= R ) 
  return true;

  return ( dx*dx + dy*dy <= R*R );
}

bool Graph::AdjMiddleFind(int n1, int n2) {

  //std::cout << "n1:" << n1 << " - n2:" << n2 << "\n";

  double intialLength = GetLength(nodes[n1], nodes[n2]);
  if(intialLength == 0)
    return 1;
 
  /*
  list<int>::iterator li;
  list<int>::iterator li2;
  for (li = adjList[n1].begin(); li != adjList[n1].end(); li++) {

    for (li2 = adjList[n2].begin(); li2 != adjList[n2].end(); li2++) {


      std::cout << "compare" << *li << " and " << *li2 << "\n";

      if (
          *li == *li2 || *li2 == n1 || *li == n2
          //&& //IF we have same nodes
          //GetLength(nodes[n1], nodes[*li]) < intialLength &&
          //GetLength(nodes[n2], nodes[*li]) < intialLength
        ) {

        std::cout << "Nodes " << n1 << " and " << n2 << " have node" <<  *li << " in common!" << "\n";
        return 1;
      }

    }
  }*/
  
  double circlePos_x =  (double) (nodes[n2]->GetNodeInfo()->GetCoordX() + nodes[n1]->GetNodeInfo()->GetCoordX()) / 2; 
  double circlePos_y =  (double) (nodes[n2]->GetNodeInfo()->GetCoordY() + nodes[n1]->GetNodeInfo()->GetCoordY()) / 2; 
  double R = (double) intialLength/2;
  
  std::vector<BriteNode*>::iterator li;
  for (li = nodes.begin(); li != nodes.end(); li++) {
    if((*li)->GetId() == n2){
      continue;
    }else if  (
      GetLength(
        circlePos_x, 
        nodes[ (*li)->GetId() ]->GetNodeInfo()->GetCoordX(),
        circlePos_y,  
        nodes[ (*li)->GetId() ]->GetNodeInfo()->GetCoordY() 
      )  < R
    )
     {
      return 1;
    }
  }
  
 
  /*
  If there is a node X between n1 and n2 which distance between n1 (n1,x) and n2 (n2,x) is less than the distance
  of (n1, n2) than do not make connection n1-n2 since these nodes are already connected over node x
  */
  /*
  std::vector<BriteNode*>::iterator li;
  for (li = nodes.begin(); li != nodes.end(); li++) {
    if( 
      GetLength(circlePos_x, circlePos_y, nodes[ (*li)->GetId() ]->GetNodeInfo()->GetCoordX(), nodes[ (*li)->GetId() ]->GetNodeInfo()->GetCoordY()   ) < R 
    ){
      return 1;
    }
  }*/



  return 0;
}

void Graph::DFS(vector<Color>& color, vector<int>& pi, int u) {

  int v;

  color[u] = GRAY;
  list<int>::iterator li;

  for (li = adjList[u].begin(); li != adjList[u].end(); li++) {
    v = *li;
    if (color[v] == WHITE) {
      pi[v] = u;
      DFS(color, pi, v);
    }
  }

  color[u] = BLACK;

}
  
} // namespace brite
